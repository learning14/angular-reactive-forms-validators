import { ValidatorFn, FormGroup, ValidationErrors } from '@angular/forms';

export const datesCompareValidator: ValidatorFn = (control: FormGroup): ValidationErrors | null => {
    const dateStart = new Date(control.get('dateStart').value).getTime();
    const dateEnd = new Date(control.get('dateEnd').value).getTime();
    console.log(dateStart);
    console.log(dateEnd);
    

    return dateStart > dateEnd ? { 'datesCompare': true } : null;
};