import { ValidatorFn, AbstractControl } from '@angular/forms';

export function datesValidator(dateEnd: Date): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } | null => {
        const DateStart = new Date(control.value).getTime();
        const DateEnd = dateEnd.getTime();
        console.log(DateStart);
        console.log(DateEnd);
        console.log(DateStart > DateEnd);
        
        return (DateStart > DateEnd) ? { 'dates': { value: control.value } } : null;
    };
}