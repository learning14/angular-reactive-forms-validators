import { Component } from '@angular/core'
import { FormControl, Validators, FormGroup, FormBuilder, ValidationErrors } from '@angular/forms'
import { datesValidator } from '../shared/directives/dates.directive'
import { datesCompareValidator } from '../shared/directives/datesCompare.directive'

@Component({
  selector: 'app-name-editor',
  templateUrl: './name-editor.component.html',
  styleUrls: ['./name-editor.component.scss']
})
export class NameEditorComponent {

  constructor(private fb: FormBuilder) { }

  dateEnd: Date = new Date('2021-01-01')
  dateStartDatesValidatorText: string

  datesForm = this.fb.group({
    dateEnd: ['', Validators.required],
    dateStart: ['', [Validators.required, datesValidator(this.dateEnd)]]
  }, { validators: datesCompareValidator })


  onDatesFormChange() {
    this.dateEnd = this.datesForm.get('dateEnd').value

    if (
      this.datesForm.errors
      && this.datesForm.errors.datesCompare
    )
      this.dateStartDatesValidatorText = 'Fechas inválidas'
    else
      this.dateStartDatesValidatorText = undefined
    // console.log(this.datesForm)
  }

  onSubmit() {
    // TODO: Use EventEmitter with form value    
  }
}